all : sieve_nostdlib

sieve: sieve.o
	gcc -o sieve sieve.o

sieve.o: sieve.c
	gcc -o sieve.o sieve.c -c -W -Wall -ansi -pedantic

sieve_asm: sieve_asm.c is_prime.o
	gcc -Wall -m32 sieve_asm.c is_prime.o -o sieve_asm

is_prime.o: is_prime.s
	nasm -f elf32 is_prime.s -o is_prime.o

sieve_nostdlib:  print.o sieve_nostdlib.c _start.o is_prime.o
	gcc -Wall -m32 -nostdlib sieve_nostdlib.c is_prime.o _start.o print.o -o sieve_nostdlib
	
print.o: print.s
	nasm -f elf32 print.s -o print.o

_start.o: _start.s
	nasm -f elf32 _start.s -o _start.o

clean:
	rm sieve sieve_asm sieve_nostdlib *.o