GLOBAL is_prime

SECTION .text
is_prime:
       ;; salvar stackframe
       pusha

       ;; salvando o valor de eax em ebx para realizar a divisao
       mov ebx, eax

       mov ecx, 2 ;; definir divisor para 2
       xor edx, edx ;; zerando o resto
       div ecx    ;; divide eax por ecx e salva o resultado em eax, e o resto em edx

       ;; armazena o valor da divisao em ecx
       mov ecx, eax

       _loop:
                ;; se ecx chegou em 1 e ainda nao achamos um divisor de eax, 
                ;; eh primo
                cmp ecx, 1
                je is_prime_end

                ;; retorna o valor original de eax
                mov eax, ebx

                ;; divide por ecx
                mov edx, 0 ;; seta o resto para 0
                div ecx    ;; divide eax por ecx, resto fica em edx

                ;; se edx is 0, divisao nao tem resto, entao ha um divisor => eax eh primo
                cmp edx, 0
                je isnt_prime_end

                ;; decrementa iterador
                dec ecx

                ;; volta pro inicio do loop
                jmp _loop

       

       isnt_prime_end:

                ;; restaura o stackframe para retornar
                popa

                ;; armazena 0 no eax (nao eh primo) para retornar
                mov eax, 0
                ret
       is_prime_end:
              ;; restaura o stackframe para retornar
              popa

              ;; armazena 1 no eax (nao eh primo) para retornar
              mov eax, 1
              ret