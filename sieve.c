#include <stdio.h>

int is_prime(int n){
  int i, cont = 1;
  for (i = 1; i <= n/2; i++){
    if (n%i == 0){
      cont++;
    }
  }
  return (cont == 2);
}

int main(){
  int n;
  scanf("%d", &n);
  printf("%d\n", is_prime(n));
  return 0;
}