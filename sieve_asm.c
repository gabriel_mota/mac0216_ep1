#include <stdio.h>

int is_prime(int n);

int main(){
  int n;
  scanf("%d", &n);
  printf("%d\n", is_prime(n));
  return 0;
}