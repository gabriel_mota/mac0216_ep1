int is_prime(int n);
void print_asm(int lenght, char* string);

// funcao que converte um vetor de caracteres em um valor inteiro
int atoi(char * str){
    int res = 0; 
    for (int i = 0; str[i] != '\0'; ++i) 
        res = res * 10 + str[i] - '0'; 
    return res; 
}

int main(int argc, char **argv){
    char mensagem[1];
    mensagem[0] = '0';
    if ( is_prime(atoi(argv[1])) ){
        mensagem[0] = '1';
    }
    print_asm(1, mensagem);
    return 0;
}