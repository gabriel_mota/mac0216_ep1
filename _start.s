GLOBAL _start

extern main

SECTION .text
_start:
    xor ebp, ebp
    pop esi
    mov ecx, esp
    push ecx
    push esi
    call main
