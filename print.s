GLOBAL print_asm

SECTION .text
print_asm: 
    push ebp
    mov ebp, esp
    sub esp, 8  ; deixando espaço para as duas variaveis

    mov edx, [ ebp+8 ] ; registrador contendo o tamanho da mensagem
    mov ecx, [ ebp+12 ] ; registrador contendo o conteudo da mensagem
    mov eax, 4 ; registrador contendo o codigo de sys_call print
    mov ebx, 1 ; reigstrador contendo o codigo da saida padrao

    int 0x80 ; chamando interrupcao de sistema

    mov esp, ebp ; retornando o stackframe
    pop ebp     
    
    mov eax, 1
    mov ebx, 0
    int 0x80
